#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
	
"""


import os
import sys


class Compte:
	
	def __init__(self, nom="unknown", identifiant=0, solde=0.0):		# Constructeur de la classe Compte avec des valeurs par défaut.
		self.nom = nom
		self.identifiant = identifiant
		self.solde = solde
		
	def afficherSolde(self):
		print("\nLe solde vaut : %f" % self.solde)


def main():
	
	mon_compte = Compte("Dupont")										# Comme le constructeur a des paramètre par défaut on peux en omettre certains.
	mon_compte.afficherSolde()
	
	return 0


if __name__ == "__main__":
	main()
