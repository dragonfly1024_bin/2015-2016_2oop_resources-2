#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
	
"""


import os
import sys


class Compte:
	
	def __init__(self, nom="unknown", identifiant=0, solde=0.0):
		self.__nom = nom
		self.__identifiant = identifiant
		self.__solde = solde
		
	def afficherSolde(self):
		print("\nLe solde vaut : %f" % self.__solde)
		
	def getNom(self):													# Accesseur (getter).
		return self.__nom;
		
	def setNom(self, nom):												# Mutateur (setter).
		self.__nom = nom
		
	def getIdentifiant(self):
		return self.__identifiant;
		
	def setIdentifiant(self, identifiant):
		self.__identifiant = identifiant
		
	def getSolde(self):
		return self.__solde;
		
	def setSolde(self, solde):
		self.__solde = solde


def main():
	
	mon_compte = Compte("Dupont", 12321, 100.5)
	mon_compte.setIdentifiant(213)										# Utilisation du setter.
	mon_compte.setSolde(20.3)											# Utilisation du setter.
	print("\n{0} {1}".format(mon_compte.getIdentifiant(),				# Utilisation du getter.
		mon_compte.getSolde()))
	
	return 0


if __name__ == "__main__":
	main()
